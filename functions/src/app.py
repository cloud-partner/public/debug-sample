import os
import boto3
from uuid import uuid4

if os.environ['LOCALSTACK_HOSTNAME']:
    DYNAMODB_ENDPOINT_URL = 'http://%s:4566' % os.environ['LOCALSTACK_HOSTNAME']
    dynamodb = boto3.resource("dynamodb", endpoint_url=DYNAMODB_ENDPOINT_URL)
else:
    dynamodb = boto3.resource("dynamodb")

table = dynamodb.Table(os.environ['TABLE_NAME'])


def lambda_handler(event, context):
    id = str(uuid4())
    item = {
        "id": id,
        "data": event['key1']
    }
    table.put_item(
        Item=item
    )

    print("value1 = " + event['key1'])
    print("value2 = " + event['key2'])
    print("value3 = " + event['key3'])
    return id
