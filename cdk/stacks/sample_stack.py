from aws_cdk import (
    Duration,
    CfnOutput,
    aws_dynamodb as dynamodb,
    BundlingOptions,
    RemovalPolicy,
    Stack,
    aws_lambda as lambda_,
)
from constructs import Construct


class SampleStack(Stack):

    def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
        super().__init__(scope, construct_id, **kwargs)

        # =========================================
        #                ddb
        # =========================================
        ddb_table = dynamodb.Table(self, "DdbTable",
                                   partition_key=dynamodb.Attribute(
                                       name="id", type=dynamodb.AttributeType.STRING)
                                   )

        # =========================================
        #                lambda
        # =========================================

        fn = lambda_.Function(
            self, 'SampleFunction',
            handler='app.lambda_handler',
            runtime=lambda_.Runtime.PYTHON_3_10,
            environment={
                'LOG_LEVEL': 'INFO',
                'TABLE_NAME': ddb_table.table_name
            },
            timeout=Duration.seconds(29),
            code=lambda_.Code.from_asset(
                '../functions/src',
                bundling=BundlingOptions(
                    image=lambda_.Runtime.PYTHON_3_10.bundling_image,
                    command=[
                        "bash", "-c", "pip install -r requirements.txt -t /asset-output && cp -au . /asset-output"],
                )
            ),
        )
        ddb_table.grant_read_write_data(fn)

        CfnOutput(
            self, 'LambdaFunctionName',
            value=fn.function_name
        )
